#!/bin/bash

#Menu

function menu() {
	echo '------------------MENU----------------
	Digite "a" para selecionar um diretorio existente
	Digite "b" para criar um arquivo com varias frases
	Digite "c" para exibir os arquivos do diretorio
	Digite "d" para exibir os subdiretorios
	Digite "e" para exibir os executaveis do diretorio
	Digite "f" para renomear todos os arquivos com letras maiusculas
	Digite "g" para renomear todos os arquivos com letras minuscilas
	Digite "h" para renomear a extensão de .txt para .docx
	Digite "i" para todos os arquivos com 3 linhas ou mais
	Digite "j" para sais do script
	'
}

while [ "$opc" != "j" ]; do
	menu
	read -p "Digite a opção: " opc

	if [ "$opc" = "a" ]
	then
		read -p "Digite o nome do diretorio: " dir
		if [ -f $dir ]
		then
			cd "$dir"
		else
			echo "Diretorio invalido"
		fi
	elif [ "$opc" = "b" ]
	then
		read -p "Digite o numero de frases: " n
		head -"$n" frases.txt > suasFrases.txt
		echo "Exibindo o arquivo"
		cat suasFrases.txt
	elif [ "$opc" = "c" ]
	then
		ls
	elif [ "$opc" = "d" ]
	then
		ls -iR
	elif [ "$opc" = "d" ]
	then
		ls -l | grep ^-..x..[x.]..[x.]
	elif [ "$opc" = "e" ]
	then
		pwd
	elif [ "$opc" = "f" ]
	then
		pwd
	elif [ "$opc" = "g" ]
	then
		pwd
	elif [ "$opc" = "h" ]
	then
		pwd
	elif [ "$opc" = "i" ]
	then
		pwd
	fi
	echo "Encerrando programa"
done
