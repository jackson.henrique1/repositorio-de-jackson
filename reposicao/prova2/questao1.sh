#!/bin/bash

#Menu

function menu() {
	echo '------------------MENU----------------
	Digite "a" para selecionar um diretorio existente
	Digite "b" para criar um arquivo com varias frases
	Digite "c" para exibir os arquivos do diretorio
	Digite "d" para exibir os subdiretorios
	Digite "e" para exibir os executaveis do diretorio
	Digite "f" para renomear todos os arquivos para letras maiusculas
	Digite "g" para renomear todos os arquivos para letras minuscilas
	Digite "h" para renomear a extensão de .txt para .docx
	Digite "i" para remover todos os arquivos com 3 linhas ou mais
	Digite "j" para sair do script
	'
}

while [ "$opc" != "j" ]; do
	menu
	read -p "Digite a opção: " opc

	if [ "$opc" = "a" ]
	then
		read -p "Digite o nome do diretorio: " dir
		if [ -d $dir ]
		then
			cd "$dir"
			echo "Diretorio $dir selecionado"
		else
			echo "Diretorio invalido"
		fi
	elif [ "$opc" = "b" ]
	then
		read -p "Digite o numero de frases: " n
		head -"$n" frases.txt > suasFrases.txt
		echo "Exibindo o arquivo"
		cat suasFrases.txt
	elif [ "$opc" = "c" ]
	then
		ls -l *.*
	elif [ "$opc" = "d" ]
	then
		ls -d */*/
	elif [ "$opc" = "e" ]
	then
		ls -l | grep ^-..x..[x.]..[x.]
	elif [ "$opc" = "f" ]
	then
		rename 'y/a-z/A-Z/' *
	elif [ "$opc" = "g" ]
	then
		rename 'y/A-Z/a-z/' *
	elif [ "$opc" = "h" ]
	then
		for file in ./*.txt; do
			mv $file "${file%.txt}.docx";
		done
	elif [ "$opc" = "h2" ]
	then
		for file in ./*.docx; do
			mv $file "${file%.docx}.txt";
		done
	elif [ "$opc" = "i" ]
	then
		echo "Tentei fazer"
		'''for file in ./*.docx; do
			until (( $(cat file | wc -l) >= 3 )); do
				rm $file
				echo "o arquivo foi apagado"
			done
		done'''
	fi
done
echo "Encerrando programa"
