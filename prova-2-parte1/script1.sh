#!/bin/bash

echo "MENU"
echo "Digite 'a' para informaçoes do hardware"
echo "Digite 'b' para verificar se o arquivo a.sh existe no diretorio atual"
echo "Digite 'c' para exibir o diretorio atual"
echo "Digite 'd' para listar os arquivos do diretorio atual"
echo "Digite 'e' para mudar o diretorio atual"
echo

while [ "${word}" != "SAIR" ]; do
	if [ "${word}" == "a" ]; then
		echo "Informaçoes do PC"
		lscpu
		echo "FIM"
		echo
	fi
	if [ "${word}" == "b" ]; then
		if test -x a.sh; then
			echo "O arquivo a.sh existe"
		else
			echo "O arquivo a.sh nao existe"
		fi
		echo "FIM"
		echo
	fi
	if [ "${word}" == "c" ]; then
		echo "Diretorio atual"
		pwd
		echo "FIM"
		echo
	fi
	if [ "${word}" == "d" ]; then
		echo "Lista de arquivos do diretorio atual"
		ls
		echo "FIM"
		echo
	fi
	if [ "${word}" == "e" ]; then
		echo "Mudar o diretorio atual"
		cd ..
		pwd
		echo "FIM"
		echo
	fi
	echo "digite SAIR para finalizar"
	read word
done
