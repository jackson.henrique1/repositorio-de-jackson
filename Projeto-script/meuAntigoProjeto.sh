#!/bin/bash

function menu() {
	while true; do
		opcao=$(zenity --width=360 --height=280 --list --title="Escolha uma das Opções" \
		       --column="Id" --column="Opção" \
		       1 "Selecione um ou mais arquivos" \
		       2 "Salvar o resultado da edição" \
		       3 "Exibir previews dos documentos" \
		       4 "Fazer merge de dois documentos" \
		       5 "Fazer split do documento" \
		       6 "Excluir páginas" )

		if [ -z "$opcao" ];
		then
			exit 0
		fi

		case "$opcao" in 
			"1" )
				marcar
				;;
			"2" )
				salvar
				;;
			"3" )
				exibir
				;;
			"4" )
				juntar
				;;
			"5" )
				dividir
				;;
			"6" )
				excluir
				;;
			*)
				exit 0
				;;
		esac
	done
}

function marcar() {
	arquivo=$(zenity --width=400 --height=320 \
		--file-selection --multiple --separator=" " \
		--title="Escolha os arquivos" \
	       	--file-filter="*.pdf")
	#configurar a seleção de mais de um arquivo

	zenity --info --title="Informativo" \
		--text="Arquivo Selecionado: $arquivo"

}

#function salvar() {


function exibir() {
	evince --preview $1 &>/dev/null &
}

function juntar() {
	saida=$(zenity --width=400 --height=320 \
		--entry --title "Nome dos arquivos de saída" \
		--text "Digite o nome do arquivo de saída: " \
		--entry-text "novoArquivo.pdf")

	pdftk $arquivo cat output $saida
	#exibir $saida
}

function dividir() {
	pdftk $arquivo burst
}


#function excluir() {


menu
