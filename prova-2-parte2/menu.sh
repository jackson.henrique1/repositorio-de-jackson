#!/bin/bash

function menu() {
	echo ' ---------->MENU<-------------
	Digite "a" para selecionar um arquivo
	Digite "b" para um breve preview do arquivo
	Digite "c" para exibir o arquivo selecionado
	Digite "d" para criar um arquivo com 10 linhas de blablabla
	Digite "e" para pesquisar algo no arquivo selecionado
	Digite "f" para criar uma copia do arqivo selecionado
	Digite "s" para sair
	'
}
#menu

while [ "$opc" != "s" ]; do
	menu
	read -p "Digite a opção: " opc

	if [ "$opc" = "a" ]
	then
		read -p "Digite o nome do arquivo: " arq
		if [ -f $arq ]
		then
			echo "$arq"
		else
			echo "Nome inválido"
		fi
	elif [ "$opc" = "b" ]
	then
		head -2 $arq
	elif [ "$opc" = "c" ]
	then
		cat $arq
	elif [ "$opc" = "d" ]
	then
		read -p "Digite o numero de linhas: " n
		head -"$n" blablabla.txt > novoblablabla.txt
		echo "Exibindo o novo arquivo"
		cat novoblablabla.txt
	elif [ "$opc" = "e" ]
	then
		read -p "Digite a palavra pesquisada no arquivo selecionado: " palavra
		grep -R "$palavra" "$arq"
	elif [ "$opc" = "f" ]
	then
		cp "$arq" novo-arq.txt
	fi
done
