#!/bin/bash

#comando que deve ser executado $ ./script1.sh um dois tres

echo "Nome do programa: $0"
echo '$0 retorna o nome do programa'
echo "O primeiro argumento passado pelo programa é: $1"
echo '$1 mostra o primeiro argumento passado pela linha de comando'
echo "O segundo argumento passado pelo programa é: $2"
echo '$2 mostra o segundo argumento passado pela linha de comando'
echo "O total de argumentos recebidos pelo programa foi de: $#"
echo '$# retorna o numero de argumentos que o programa recebeu'
echo "Todos os argumentos passados foram: $*"
echo '$* retorna todos os argumentos informados na execução do programa'

