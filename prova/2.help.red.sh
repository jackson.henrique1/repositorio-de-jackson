#!/bin/bash

echo -e "Redirecionadores\n"
sleep 2
echo -e "O '|' conecta a saída do comando anterior com a entrada do seguinte\n"
sleep 2
echo -e "O '> e >>' redirecionam a saída padrão do comando para um arquivo\n"
ls -l > testesobrescreve.txt
pwd >> testeadicionafim.txt
sleep 2
echo -e "Sendo que o '>' sobrescreve o arquivo em questão, enquanto que o '>>' adiciona a saída do comando ao final do arquivo sem apagá-lo\n"
sleep 2
echo -e "O '2> e 2>>' redirecionam a saída padrão de erros do comando para um arquivo, sobrescrevendo o arquivo ou adicionando a saída ao final do arquivo, respectivamente\n"
sleep 2
echo -e "O '&> e &>>' redirecionam todas as saídas do comando para um arquivo, seguindo as definições para '> e >>', descrito anteriormente\n"
sleep 2
echo -e "O '<' redireciona o arquivo para a entrada padrão do comando\n"
sleep 2
echo -e "O '<<' permite redirecionar a entrada padrão do comando para o documento escrito no bash\n"
sleep 2
echo -e "O '<<<' permite redirecionar a entrada padrão do comando para a string escrita no bash"
