#!/bin/bash
while : ; do
    options=$(yad     --list --fixed --center --borders=10 --window-icon="./images/panda.png" --title="Pandinha PDFTools" --columns=2 --no-buttons --image-on-top --image=./images/panda.png  \
                        --text "O que deseja fazer?"\
                        --column "Opção" --column "Descrição"\
                        --width="750" --height="750" \
                        1 "Escolher documentos para Editar" \
                        2 "Exibir Preview" \
                        3 "Dividir documento" \
                        4 "Dividir um documento por página" \
                        5 "Juntar documentos" \
                        6 "Excluir Páginas" \
                        0 "Sair" )

        options=$(echo $options | egrep -o '^[0-9]')

        # De acordo com a opção escolhida, dispara programas
        case "$options" in
            "0") exit;;
            "1") FORM=$(
                yad --center --title="Selecione o local do Arquivo "            \
                    --width=400 --heigth=400                                    \
                    --form                                                      \
                    --field="PDF-1  : " ""                                       \
                    --field="PDF-2  : " ""                                       \
                    --button="Adicionar"                 \
                    )
                PDF-1=$(echo "$FORM" | cut -d "|" -f 1)
                PDF-2=$(echo "$FORM" | cut -d "|" -f 1)
                echo $FORM;;
	"2") exibir;;
	"3") ;;
	"4") dividir;;
	"5") juntar;;
	"6") exclur;;
        esac
done

'''function marcar() {
	arquivo=$(zenity --width=400 --height=320 \
		--file-selection --multiple --separator="|" \
		--title="Escolha os arquivos" \
	       	--file-filter="*.pdf")
	#configurar a seleção de mais de um arquivo

	zenity --info --title="Informativo" \
		--text="Arquivo Selecionado: $arquivo" \
}'''

#function salvar() {


function exibir() {
	evince -w PDF-1
	evince -w PDF-2
}

function juntar() {
	saida=$(yad --center --title="Arquivo de Saída" \
	       	--width=400 --height=320 \
		--entry --text "Digite o nome do arquivo de saída: " \
		--entry-text "novoArquivo.pdf")

	pdftk PDF-1 PDF-2 cat output $saida
	evince -w $saida
}

function dividir() {
	pdftk PDF-1 burst
}

function excluir() {
	FORM=$(yad --center --title="Selecione o local do Arquivo "            \
                    --width=400 --heigth=400                                    \
                    --form                                                      \
                    --field="PDF : " ""                                       \
                    --button="Adicionar"                 \
                    )
                PDF=$(echo "$FORM" | cut -d "|" -f 1)

	pagina=$(yad --center --title="Escolher Paginas" \ 
		--width=400 --height=320 \
		--entry --text "Digite o numero das paginas que deseja excluir" \
		--entry-text "1-4 10,12")

	pdftk $PDF cat $pagina output resultado.pdf
	evince -w resultado.pdf
}

$menu

